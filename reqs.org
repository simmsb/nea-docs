#+TITLE: NEA PROJECT - requirements only
#+AUTHOR: Ben Simms
#+EMAIL: ben@bensimms.moe
#+BIND: org-odt-preferred-output-format "pdf"

#+NAME: export-set
#+BEGIN_SRC emacs-lisp :exports none
(setq org-export-directory (expand-file-name "./renders")
      org-odt-preferred-output-format "pdf"
      org-src-fontify-natively t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (plantuml . t)
   (ditaa . t)))
(require 'irony)
#+END_SRC

#+RESULTS: export-set
: irony

#+CALL: export-set() :exports none

* Project requirements
** Language requirements
The features of the programming language should be similar to the features of C,
following a procedural and imperative design, with support for variables,
arrays, pointers, functions (and function pointers).

The syntax of the programming language should be C-like, with few abstractions.
However I do not want to bring in parts of the C syntax that are considered bad,
for example the syntax for declaring functions and arrays gets messy very
quickly, to declare the variable ~x~ as an array of function pointers to
functions taking two ints and returning an int you would have to use: ~int
(*x[])(int, int)~. This syntax requires you to learn rules the [[http://ieng9.ucsd.edu/~cs30x/rt_lt.rule.html][right to left
rule]][fn:rtlr] and [[http://c-faq.com/decl/spiral.anderson.html][spiral rule]][fn:spiralrule] to be able to read relatively
simple declarations. Instead the type declaration syntax will be based off of
those that are found in modern languages such as [[https:www.Rust-lang.org][Rust]][fn:rust] and
[[https:www.Haskell.org][Haskell]][fn:haskell], which have simple and easy to read type declaration syntax.
In my syntax, the declaration exampled above would instead be ~x: [(u2, u2) ->
u2]~, where ~[type]~ declares an array of the inside type, and ~(type, ...) ->
type~ declares a function taking parameters of some types and returning a type.
Variable declarations will also support type inference, given the declaration
~var a := {1, 2};~ the type of ~a~ should be inferred to being an integer array
of length 2 (~[u1@2]~).

The language will require an implementation of a function named ~main~ that
takes no variables and returns ~Void~, this function will be the entry point of
the program.

Other parts of the C syntax that wont be included will be if statement and loop
bodies that are not new blocks, preventing errors such as:

#+CAPTION: The call to the function =fn_b= is not dependant on the above if statement, even if the indentation suggests it is
#+BEGIN_SRC c
if (a < b)
    fn_a();
    fn_b();
#+END_SRC

Instead, all bodies of conditionals must be new block scopes, preventing the
above logic error.

The core features of the language are as follows:

*** <<feature-number_literals>>Numeric literals
Numeric literals will be representable in the language as expressions.

The syntax for integer literals is ~[-]{number}[/{type}]~, with optional sign
and type. Some examples are: ~1~, ~-1~, ~1/u8~, ~1/s8~, ~-1/s8~.

Singular characters are also integer literals, for example ~'a'~ becomes the
integer literal ~97~.
*** <<feature-arrays>>Array literals
Array literals are used to construct arrays of data.

The syntax of array literals is: ~{e0, e1, ...}~.

Arrays can be either as literals or as variables, variable arrays can be
initialised with array literals.

Elements of array literals can be any expression and are built up at runtime, 

#+BEGIN_SRC rust
var a := {1, 2, 3} // valid. type of a is *u1
var b := {1 + 2, 3} // valid
var c : [u1] = {1 + 2, 3} // valid, type of c is [u1@2]
var d : *u1 = {1 + 2, 3} // invalid, type of d is *u1
#+END_SRC

The type of array literals is such that the outermost array decays to a pointer.
The literal ~{{1, 2}, {3, 4}}~ has type ~*[u1]~. (unless used in an initialiser
to an array, in which case the type would be ~[[u1]]~)

When a variable is declared an array type with no length, the length is
calculated from the initialiser.

<<feature-string_literals>>String literals are also array types, ~"hello world"~
has type ~*u1~. String literals become null-terminated (an extra byte is added
to the end with value of =0= to signify the end of the string) arrays of ~u1~.
*** Variables
Variables serve as identifiers for data, they compose an identifier (the name to
access the variable), a type (which determines the rules on how the variable is
used), and location information.

Variables will be both global and local, local variables will exist in only
their scope and child scopes and will be destroyed/ made inaccessible once their
scope is left.

Variables will be declared with the form:
#+CAPTION: Inferred types.
#+BEGIN_SRC rust
var name := initialiser;
#+END_SRC

#+CAPTION: Defined type but no initialiser.
#+BEGIN_SRC rust
var name : type;
#+END_SRC

#+CAPTION: Defined type with initialisation.
#+BEGIN_SRC rust
var name : type = initialiser;
#+END_SRC

Where the initialiser is any expression.

After a variable has been declared in a scope it can be referenced by using the
given name, the result of using the identifier will be the value of the
variable, except in the case of arrays and functions declared using function
definition syntax, where the value of the array identifier is a reference to the
first element of the array or in the case of functions, the identifier is a
reference to the start of the procedure in memory.

Identifiers except those of arrays and functions carry lvalues, allowing them to
be used as operands to the reference to operator (~&~) and to the left hand side
of the assignment operator (~=~).
*** <<feature-globals>>Global variables
Global variables are created by any toplevel (not inside function) variable declaration. For
example, to declare a global variable named ~a~ of type ~u8~, you would write:

#+BEGIN_SRC rust
var a : u8;
#+END_SRC

Global variables are readable and writable (permitting they are not of const type) in all scopes.
*** <<feature-variables>>Local variables
Variables that exist only inside their creation scope and child scopes.

If declared in a function scope the variable will become local to the function scope and will only exist inside the function body.
If declared in a block scope the variable will be local to the block scope and will not be visible in any outside scope.
Variables declared in subscopes can shadow (have the same name as) variables in outer scopes, is a shadowing variable is declared in a subscope the shadowed variable will be visible up until the point of the declaration of the shadowing variable.
*** Types
Types describe how a variable or expression can be used and represented in memory.

The following types are supported, the semantics of them will be talked about later:
 - Integer types :: Declared as ={sign}{size}=, for example: =s2= and =u8= to
      declare a signed 2-byte integer and an unsigned 8 byte integer
      respectively.
 - Function types :: Declared as =(arg_types, ...) -> return_type=, for example
      =(u8, u8) -> u8= would be a function taking two arguments of type u8, and
      returning a u8.
 - Array types :: Declared as =[type]= or =[type@size]=, for example: =[u8@4]=
      to declare an array of inner type =u8= with space for 4 elements. the size
      specifier will not required if initialising with an array. The size of an
      array is equal to the size of it's element type multiplied by the length
      of the array.
- Pointer types :: Declared as =*type=, declares a pointer to a type, that is: a
     value that references a memory location containing the pointed to type. For
     example: =*u8= declares a pointer to a =u8=.
- Void type :: A special type, represented as =()=, variables cannot be of void
     type and expressions that have type void are unusable inside other
     expressions. Pointer arithmetic cannot be performed on pointers of void
     type, however void pointers may freely be casted into any other pointer
     type without an explicit cast or a compiler error.

Additionally the const modifier =|type|= can be used to prevent assignment to a
type. If an assignment operation attempts to assign to something of const type,
the program will fail to compile and a relevant error message will be displayed.

The type cast expression can be used to forcibly change the type of an
expression. for example:

#+BEGIN_SRC rust
var a : *u8 = 10::*u8;
#+END_SRC
Which changes the type of the integer literal ~10~ from a ~u1~ (single byte
unsigned integer) into a ~*u8~ (pointer to 8 byte unsigned integer)

The =sizeof= expression will be used to get the size of a type or expression in
bytes, for example:
#+BEGIN_SRC rust
sizeof<u8>
#+END_SRC
Will yield the value =8=, and:
#+BEGIN_SRC rust
sizeof<[u8@10]>
#+END_SRC
Will yield the value =80= (base type * size of array).

Sizeof can be used on expressions, for example:
#+BEGIN_SRC rust
var a : *u8;
sizeof<a>  // returns the size of a pointer to u8, 2 bytes
sizeof<*a>  // returns the size of a u8, 8 bytes
#+END_SRC

Expressions used in sizeof expressions are not evaluated, the following will
have no effect on the program:
#+BEGIN_SRC rust
fn a() -> u8 {
    crash_program();
}

sizeof<a()>
#+END_SRC

*** <<feature-functions>>Functions
In the language it will be possible to describe functions that take parameters
and return a result that can be used in other expressions. Functions will only
be declarable at top level (as allowing functions to be defined inside other
functions complicates the compilation process) in the form:
#+BEGIN_SRC rust
fn function_name(arg1: type1, arg2: type2) -> return_type {
    // function body
    return arg2[arg1];
}
#+END_SRC

The arguments to a function will become variables visible in the scope of the
function body that reference the arguments passed to the function when called.

Functions will be called/ invoked using syntax shared by many other languages.

#+BEGIN_SRC rust
function_name(argument_0, argument_1)
#+END_SRC

Function calls are expressions, the return value of the function becomes the
value of the expression.

Inside the body of the function the ~return {expr}~ statement can be used to
return a value from the function. Once a return is reached execution inside the
function body stops and the return value is passed to the caller. The value
returned must be castable to the declared return type of the function.

The return statement can be used without an expression when used inside a
function of void return type (~() -> ()~).

Functions have the type ~(arg, arg2, arg3) -> return_type~, when calling a
function the types of the arguments must match (or can be implicitly casted to
the correct type) and the type of the call expression will be the return type of
the functions. In the case of functions that have a void return type (~() -> ()~) the
expression will also have void type and will thus be will not be usable inside
any other expression.

Functions will also be able to declare themselves as accepting a variable number
of arguments by adding a series of 3 periods as an extra argument to the
function, for example: ~fn printf(str: *u1, ...)~ which declares a function
taking one argument of type =pointer to u1= and then any number of arguments following.

When a function is declared as varargs no typechecking occurs on the extra
arguments and it is up to the caller to make sure the types are correct or
unexpected behaviour may occur.

When a function is declared to have varargs an extra identifier will be usable
inside the function body with the name: ~var_args~ and the type ~*()~ (pointer
to void). The variable will be read only and will point to the last of the
function parameters. To access varargs this pointer will have to be casted to a
non-void pointer type and the index of the vararg (starting from 1) to read
subtracted.

For example, to read the first vararg passed assuming the argument was a ~u8~:
#+BEGIN_SRC rust
fn test(...) {
    var arg := *(var_args::*u8 - 1);
}
#+END_SRC
*** <<feature-assignment>>Assignment operator
Expressions with LValues (have a storage location) are able to be 'assigned' to
in an assignment expression, setting a memory address determined by the left
hand side to the value of the right hand side.

The syntax for assignment is the ~=~ operator, in the form ~lhs = rhs~. For example:

#+BEGIN_SRC rust
var a : u8;
a = 4;
#+END_SRC

The assignment operator is an expression, and it's value will be the rhs of the
operation, meaning the following sets both ~a~ and ~*b~ to 4:

#+BEGIN_SRC rust
a = (*b = 4);
#+END_SRC
*** Binary operators
Binary operators take the form ~lhs {op} rhs~ and apply a binary operation to
two operands, yielding a result. They are parsed with a precedence, such that
~a + b * c~ is parsed as ~a + (b * c)~. The precedence of an operator determines
if it will be evaluated before surrounding expressions, a lower precedence means
the operator is evaluated before surrounding expressions that have higher
precedence. Binary operators also have an associativity, which determines
whether operators of the same precedence parse from the right or left, a left
associative operator parses ~a * b * c~ as ~((a * b) * c)~ while a right
associative operator parses as ~(a * (b * c))~.
**** <<feature-bitwise>>Bitwise operators
The bitwise operators: ~|~, ~^~, ~&~ are =or=, =xor=, and =and= respectively.
They perform a bitwise operation on their two operands. For example ~1 ^ 2~
yields ~3~.

These operators are left associative.
**** <<feature-ss_ops>>Logical operators
The logical operators: ~||~, ~&&~ are =or=, and =and= respectively. Logical =or=
evaluates the left operand, and if it is nonzero, yields the result. If the lhs
is zero, the rhs is computed and is the result of the expression. Logical =and=
evaluates the left operand, and if it is zero it, yields the result. If the lhs
is nonzero, the rhs is computed and is the result of the expression.

These  operators are right associative.
**** <<feature-comparison>>Relational operators
The relational operators: ~==~, ~!=~, ~<~, ~<=~, ~>~, ~>=~ are =equal=,
=not-equal=, =less-than=, =less-than-or-equal=, =more-than=, and
=more-than-or-equal= respectively. Relational operators compute a comparison of
the left and right operand and return a boolean (1 or 0) result depending on the
comparison. For ~==~ and ~!=~ the sign of the operands is not significant. for
the rest: the presence of a signed operand forces both operands to become
signed.

These operators are left associative.
**** Bitwise shift operators
The bitshift operators: ~<<~, ~>>~ are =left-shift=, and =right-shift=
respectively. The result of the operation ~lhs << rhs~ is ~lhs~ shifted left by
~rhs~ bits, similarly for ~lhs >> rhs~ where ~lhs~ is shifted right by ~rhs~
bits. In this operation the right hand side is implicitly casted to unsigned
type. In a right shift operation, if the left hand side is unsigned the result
is padded with zeros, if signed the result is padded with ones.

These operators are left associative.
**** <<feature-math>>Numerical operators
The numerical operators: ~+~, ~-~, ~*~, ~/~ are =add=, =subtract=, =multiply=,
and =divide= respectively. These operators are left associative.
*** Unary operators
Unary operators take the form ~{expr}{op}~ for postfix operators and
~{op}{expr}~ for prefix operators. Prefix operators have a lower precedence than
binary operators, and postfix operators have a lower precedence than prefix. The
existing unary operators are:

**** Prefix operators
***** Dereference
This operator: ~*~, dereferences a pointer value yielding the value that was
pointed to. It is the opposite to ~&~ which provides a pointer to a expression
that has a LValue. An example that manipulated data using pointers:

#+BEGIN_SRC rust
var a : u8 = 4;
var b := &a;
*b = 5;

*b // yields 5
a // yields 5 aswell
#+END_SRC


***** Pre increment
The operators ~--~ and ~++~ decrement and increment an expression with a lvalue
and yield the incremented result.

an example:
#+BEGIN_SRC rust
var a := 1;

++a; // yields 2
a // yields 2 aswell
#+END_SRC
***** Inverting operators
The operators ~~~, ~!~, ~-~, and ~+~ perform inverting operations.

- ~~~ :: Bitwise negate, every bit of expression is flipped.
- ~!~ :: Logical negate, ~0~ becomes ~1~, ~1~ becomes ~0~.
- ~-~ :: Numerical negate, returns the negated value, ~-1~ becomes ~1~. Cannot
     be used on unsigned expressions.
- ~+~ :: Numerical absolute, returns the positive value of a signed expression,
     ~-1~ becomes ~1~, ~1~ stays as ~1~. This is a No-op on unsigned
     expressions.
**** Postfix operators
- ~{expr}({expr}, ...)~ :: Function call expression, calls a function with
     parameters.
- ~{expr}[{expr}]~ :: Array index expression, indexes into a pointer or array.
- ~{expr}++~, ~{expr}--~ :: Postfix increment, decrement. Acts like prefix
     increment and decrement but the unincremented value is returned.
- ~{expr}:::{type}~, ~{expr}::{type}~ :: Type cast, ~:::~ reinterprets the bit
     pattern of the data and does not resize the type to fit, ~::~ resizes the
     type and will reinterpret the bit pattern.
*** <<feature-pointers>>Pointers
In a low level language such as this language, it is a useful feature that
memory locations should be able to be passed around and dereferenced.

The syntax of a pointer type is ~*{type}~ which declares a =pointer to type=.
The prefix operator ~*~ is the dereference operator and retrieves the memory
that the pointer points to. The type of a dereference expression is the type of
what the pointer type pointed to, for example: given the expression: ~*a~ (where
~a~ is of type ~*u1~), the result is the memory that ~a~ pointed to and is of
type ~u1~.

The prefix operator ~&~ is the 'pointer-to' operator and is used to get a
pointer to any expression that has a LValue (known storage location), the type
of the expression is a pointer to the type of the expression it was used on, for
example: given the expression: ~&a~ (where ~a~ is of type ~u1~), the result is a
pointer to the memory location that the variable ~a~ has and is of type ~*u1~.

The result of the expressions ~*&x~ and ~&*x~ are both no-ops (they do nothing
and only yield ~x~) providing that in the first: ~x~ has a LValue, and in the
second: ~x~ is of pointer type.

**** Pointer arithmetic
It will be possible to use pointer types in binary expressions.

In a binary add (~+~) expression where one operand is a pointer type and one
operand is an integer type, the result is another pointer type that has the
value of the pointer with the other operand added multiplied by the size of
pointed to type to it, this is used to facilitate array indexing: the expression
~a[b]~ is semantically synonymous with ~*(a + b)~.

In a binary subtract (~-~) expression where both operands are pointer types, the
result is an integer type that represents the distance in memory between the two
pointers.
*** Arrays
Arrays are a consecutive layout of their contained type, they support indexing
to retrieve elements of the array. Array types are ~[{type}]~ or
~[{type}@{size}]~. Array types can compose other arrays, allowing for
multi-dimensional arrays.

The postfix array-index operator ~{expr}[{index}]~ is used to index arrays. For
example: given an array ~a~ of type ~[u1]~, ~a[0]~ retrieves the first element
of the array which is of type ~u1~. Array indexes can be chained for indexing
multi-dimensional arrays, for example: given an array ~b~ of type ~[[u1]]~,
~b[0][1]~ retrieves the first element of the array (type ~[u1]~), and then
retrieves the second element of that array, of type ~u1~.

Variables initialised with array types reference contiguous arrays of memory, use of an identifier of a variable that has an array types yields a value with the outer array type replaced with a pointer (that is: ~[[u8]]~ is replaced with ~*[u8]~)

Outside of variable declarations, array types are not allowed to be an outer type. This means that function parameters cannot have array type, instead they should have pointer type.

Instead of:
#+BEGIN_SRC rust
fn a(b: [[u8@3]@3]) {
}
#+END_SRC

The function declaration should be:
#+BEGIN_SRC rust
fn a(b: *[u8@3]) {
}
#+END_SRC

Pointer types have similar semantics to array types, the only difference being that the pointer type does not hold the size of the array.
*** <<feature-if_stmt>>IF statements
IF statements are used to select paths in a program. While loops are used to
perform repetition.

IF statements are composed of a single condition, followed by 0 or more ~elif~
conditions, optionally followed by an else condition. If the first condition is
true (nonzero), it's body is computed and then control flow resumes after the if
statement. If the first condition is false each elif condition is tested (if
they exist) in turn. If none of the conditions were true and an else case is
present, the body of the else case is computed.

IF statements have the syntax:
#+BEGIN_SRC rust
if condition {
    if_true;
} elif condition {
    elif_false;
} else {
    else_condition
}
#+END_SRC
*** <<feature-loop_stmt>>While loops
While loops consist of a condition and a body, control flow passes over the body
of the loop while the condition is true. When control flow enters a while loop
the condition is tested, if false control flow skips outside the loop. When the
end of the loop is reached control flow jumps to the start of the loop again.

While loops have the syntax:
#+BEGIN_SRC rust
while condition {
    // ...
}
#+END_SRC
*** Modules
Modules will be used to create namespaces inside files.

The syntax for modules will be:
#+BEGIN_SRC rust
mod name {
    // ...
}
#+END_SRC

To access a function or variable that is inside a module, the identifier should be prefixed with the name of the module and a period, to access function ~b~ that is declared inside module ~Lib~ you would use ~Lib.a~

To access an element of another module (or a parent module) from another module, the identifier should be prefixed with ~..~ which will bring the namespace of the identifier to the outer level and start from there.

Modules contain declarations and prepend their name to all identifiers declared, for example:
#+BEGIN_SRC rust
mod Lib {
    fn a() {
        ..test();
    }

    fn b() {
        a();
    }
}

fn test() {}

fn main() {
    Lib.b();
}
#+END_SRC
*** Inline Assembly
To allow for interfacing with IO components of the VM, It will be necessary to
allow for inline assembly to be embedded in programs.

The syntax for inline assembly should be:
#+BEGIN_SRC
_asm [ instr; instr; ... ] { capture1, capture2, ... } ;
#+END_SRC

Where the syntax for an asm instruction is the name of the instruction followed by parameters:
#+BEGIN_SRC rust
//  a register
instruction_name:size, param0, param1, ...
#+END_SRC

Instruction params can be registers, immediates, dereferences or the values of captured expressions.
#+BEGIN_SRC rust
// Register capture
%{register index}@{size}

// immediate
#{immediate_value}@{size}

// Expression capture
<{expression index}>

// dereference
​*{parameter}
#+END_SRC

Dereferences can be followed by the rest of the parameter, the following would dereference the literal ~100~:
#+BEGIN_SRC rust
*#100
#+END_SRC

'Expression captures' mean to reference the value of an expression in the expression capture block of the asm literal, the syntax ~<0>~ means to capture the value of the first expression (0-index) of the captured expressions.

#+CAPTION: This example would compile to an instruction calling ~putc~ with the value of ~'a'~.
#+BEGIN_SRC rust
_asm [ putc:1, <0>; ] { 'a' };
#+END_SRC

#+CAPTION: Another example, the following will write ~10~ to the memory address ~100~
#+BEGIN_SRC
_asm [ mov:8, *<0>, <1> ] { 100, 10 };
#+END_SRC
** Compiler requirements
The compiler will take in a source code file, which will then be processed
through each stage of compilation and then be emitted as a binary that can be
executed on the virtual machine.
*** User interface
The user interface of the compiler will be a simple command line interface, with
command line options and flags to control what the compiler will do.

The interface should have options for displaying the Intermediate representation
of the program, along with the resultant machine instructions to aid in
debugging when writing the compiler.

The compiler should also produce some statistics about the compiled program,
such as the number of functions compiled and the size of the program binary.
*** Compilation stages
There is 3 main stages of the front and middle end of the compiler and 2 of the
backend, the result of each stage is passed onto the next after completion. The
compiler must be able to complete all of these to be complete.

- Parser :: The parser will take the program source and emit an abstract syntax
     tree that represents it. An abstract syntax tree (usually shortened to:
     AST) is a tree data structure that is traversed by the compiler to build
     the output program. The type information of expression nodes is contained
     on the tree.
- Type checker :: The types of expressions are checked for validity, the types
     of expressions is also used to infer the type of variables when
     initialised.
- IR generate :: Given an AST, generate a block of IR (intermediate
     representation) that consists of instructions to load and store variables,
     move data between registers, manipulate data and form branch points in
     code. The IR should be abstract enough to allow for multiple backends to be
     possible, allowing in the future a LLVM[fn:llvm] backend to be written.
- Register allocate :: The IR generated by the compiler will operate on an
     abstract machine that has an infinite amount of registers, however the
     target VM does not, and so a pass of register allocation will have to
     occur, where each IR instruction has the registers it uses mapped to
     physical register on the virtual machine. If there is too many physical
     registers mapped to 'abstract' registers, then physical registers will have
     to be 'spilled' to temporary places in memory.
- Machine code emit :: After allocating registers, the IR can be transformed
     into instructions that correspond with those of the VM and encoded into
     bytes that can be placed into a binary, all literals and global variables
     will be allocated a place in the binary. At this stage all references will
     also be resolved into absolute locations.
                  
There are also two optional requirements that can be added to the compiler.

- Optimisations :: At several stages of the compiler, optimisations can be
     applied to the program to increase speed and reduce the required
     computations.
- Source-Output visualiser :: It should be an option for the compiler to emit
     data relating each line of assembly to a relevant line in the source code,
     providing insight into the compiler internals.
*** Compiler Errors
If the program that the compiler is asked to compile is invalid in any way, the
compiler should emit an error describing the reason for the error, and also
highlight the region of the program that cause the error to be produced. For
example, attempting to dereference (read a location in memory) an expression
that is not of a pointer type should cause an error to be produced. Attempting
to compile this code should result in an error:

#+BEGIN_SRC rust
def x(a: u8) -> u8 {
    return *a;
}
#+END_SRC

#+begin_example
ERROR: Attempt to dereference non-pointer type
On line: 1

return *a;
       ~~~
#+end_example

The compiler should produce informative errors when both parsing and during the
code generation. Syntax errors should be pointed out with a marker that marks
out the location of the syntax error and errors during code generation should
mark out the region the error occured in along with informative information on
why the error occured.

The compiler should not produce any errors that are not intended for the end
user (someone wanting to compile a program with the compiler), extensive testing
will be used to ensure that only errors intended for the user are ever produced.
*** Optimisation requirements
An optional requirement to the compiler should be it's ability to apply
optimisations to the program being compiled. As optimisations do not affect the
completeness of the compiler they are all extra requirements and may not be
implemented.
Each optimisation the compiler could apply are listed here with
their descriptions.

- Constant Folding :: If two operands to an expression are constant, they can be
     reduced to a single expression inside the compiler, for example: changing
     ~(1 + 2)~ into ~3~
- Common sub-expression elimination :: If an expression contains multiple
     sub-expressions that can be proved to be identical, the computation can be
     eliminated with the compiler sharing the result, for example ~(a + b) *
     (a + b)~ can be reduced to ~%1 = (a + b), (%1 * %1)~ where ~%1~ is a
     temporary variable.
- Tail Call Optimisation :: If the last statement in a function is a recursive
     call to itself, update the parameters and jump back to the start of the
     function instead of creating a new stack frame.
*** Position independant compilation
The compiler should be able to compile a program that has functions containing
mutual dependencies without the requirement for function prototypes.

For example, the following program should compile even though both functions
have mutual dependencies on each other:

#+BEGIN_SRC rust
fn is_even(x: u8) -> u1 {
    if x == 0 {
        return 1;
    }
    return !is_false(x - 1);
}

fn is_false(x: u8) -> u1 {
    if x == 1 {
        return 1;
    }
    return !is_true(x - 1);
}
#+END_SRC
*** Standard library
The compiler should provide a standard library for use in programs, It should
include features such as printing strings to stdout, reading in of information
and printing of integer types.
** Virtual machine requirements
The virtual machine should be able to read in a binary file that is produces by
the compiler to execute, then run the instructions contained inside until
complete.

The VM will use a risc architecture with the following instruction groups:
| Instruction type | Description                                                    |
|------------------+----------------------------------------------------------------|
| Move             | Move data between registers and memory.                        |
| Math             | Apply mathematical and bitwise operations to operands.         |
| Test             | Compare operands and set status registers.                     |
| Jump             | Conditionally jump to locations depending on status registers. |
| Stack            | Stack manipulation operations, push, pop, etc                  |

To aid in testing the VM program should be able to take optional arguments that
specify a region in memory to read from after the program has finished running,
the value that was read should then be compared with another parameter for
equality. The idea being that if the read value matches the value given to the
VM then the program was correct, and any other value means that the compiler
produced an incorrect program. Care will have to be taken to ensure that the VM
and the program that the compiler will compile are both correct so that the
blame of the failure can only be because of the compiler producing incorrect
code.
** Optional Programming Language requirements
Extra non-essential features of the language that are only syntax sugar are listed below.
Implementation of these features does not effect the completeness of the language and so will all be extras.

- Structures (product types)                          :: Types that can be
     composed to produce meaningful groups of data.
- Unions (sum types)                                  :: Types that can be
     joined to produce a type that can hold one piece of data but represent many
- File inclusion                                      :: Able to include the
     functions and types contained in other source files.
- Named loops                                         :: For and while loops
     should be able to be named, and break statements should be able to jump to
     specified loops (defaulting to the closest).
- Break/ Continue statements                          :: Statements that cause
     control flow to jump to the end/ start of a loop.
- Ranged for loops                                    :: For loops over a range
     of numbers given a start, end and step.
- Type inspection                                     :: Getting the type of a
     variable should be possible in the type system.
- Polymorphic functions (Argument type)               :: Functions that are
     polymorphic to arguments should be possible.
- Polymorphic functions (Return type)                 :: Functions that are
     polymorphic to the return type.
- Polymorphic Type variables                          :: For polymorphic
     Structs, Unions and Functions, Type variables used to constrain types.

* Footnotes

[fn:pytest] [[https://docs.pytest.org/]]

[fn:llvm] [[https://llvm.org]]

[fn:spiralrule] [[http://c-faq.com/decl/spiral.anderson.html]]

[fn:rtlr] [[http://ieng9.ucsd.edu/~cs30x/rt_lt.rule.html]]

[fn:cpython] [[https://github.com/Python/cPython]]

[fn:elixir] [[https://elixir-lang.github.io]]

[fn:python] [[https://Python.org]]

[fn:racket] [[https://racket-lang.org]]

[fn:haskell] [[https://www.Haskell.org]]

[fn:rust] [[https://www.Rust-lang.org]]

[fn:three-address-code] https://en.wikipedia.org/wiki/Three-address_code
