(TeX-add-style-hook
 "doc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org97e21e5"
    "sec:org13bb46c"
    "sec:orgd9dcec2"
    "org2c715f5"
    "orgeca20dd"
    "org37d5fcc"
    "sec:org7074017"
    "sec:orga241de3"
    "org0ce5ea7"
    "sec:orgebbb967"
    "sec:orgb9b33c1"
    "sec:orgd7e35f3"
    "sec:orgc350977"
    "sec:org51a3b3c"
    "sec:org06e7995"
    "sec:orgefdbdc9"
    "sec:org58abdc0"
    "sec:org88b0426"
    "org6122975"
    "org3e8da6f"
    "org72c72c2"
    "org6063903"
    "org62d0b3a"
    "orgb6ac658"
    "orgcd42724"
    "org7a25d44"
    "orgda82184"
    "org6cf08b4"
    "orgce6b6fb"
    "org3b4c6f9"
    "org3c88389"
    "org484efb2"
    "sec:org5b0ca6b"
    "sec:orga7a9e5a"
    "sec:orgbbf9965"
    "sec:org806c73d"
    "sec:orgb4d77e4"
    "sec:orge870b92"
    "sec:org9180911"
    "sec:orgcf58413"
    "sec:orgebc5b69"
    "sec:orgd52e8c3"
    "sec:orgaee4205"
    "sec:org4a1ee73"
    "sec:org0a33f27"
    "sec:org64ed43c"
    "sec:org8ecd780"
    "sec:orgc26a022"
    "sec:org4a1c2da"
    "sec:orgebd2c75"
    "sec:org8c65742"
    "sec:orgfb8e3a2"
    "sec:org6c308f5"
    "sec:org3801309"
    "sec:orgb414d4c"
    "sec:orge7efcd0"
    "sec:org7fd8238"
    "sec:org64a7311"
    "sec:org14f13f9"
    "sec:org1a44084"
    "sec:orga929749"))
 :latex)

